const mongoose = require('mongoose')
const Schema = mongoose.Schema
const userSchema = new mongoose.Schema({
  name: String,
  surname: String,
  age: Number,
  type: String
})

module.exports = mongoose.model('User', userSchema)
