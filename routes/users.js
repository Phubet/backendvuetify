const { json } = require('body-parser')
var express = require('express')
var router = express.Router()
const User = require('../models/User')

/* GET users listing. */
router.get('/', async function (req, res, next) {
  try {
    const users = await User.find({})
    res.json(users)
  } catch (err) {
    res.status(500).sent(err)
  }
})

router.get('/:id', async function (req, res, next) {
  const { id } = req.params
  try {
    const users = await User.findById(id)
    res.json(users)
  } catch (err) {
    res.status(500).sent(err)
  }
})

router.post('/', async function (req, res, next) {
  const payload = req.body
  const user = new User(payload)
  try {
    await user.save()
    res.json(user)
  } catch (err) {
    res.status(500).sent(err)
  }
})

router.put('/', async function (req, res, next) {
  const payload = req.body
  try {
    const user = await User.updateOne({ _id: payload._id }, payload)
    res.json(user)
  } catch (err) {
    res.status(500).sent(err)
  }
})

router.delete('/:id', async function (req, res, next) {
  const { id } = req.params
  try {
    const user = await User.deleteOne({ _id: id })
    res.json(user)
  } catch (err) {
    res.status(500).sent(err)
  }
})

router.post('/user/login', async function (req, res, next) {
  const name = req.body.name
  const surname = req.body.surname
  try {
    const users = await User.find({ name: name, surname: surname })
    res.json(users)
  } catch (err) {
    res.status(500).sent(err)
  }
})

module.exports = router
